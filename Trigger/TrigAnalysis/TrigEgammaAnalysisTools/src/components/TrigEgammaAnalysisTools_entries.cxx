#include "TrigEgammaAnalysisTools/TrigEgammaNavAnalysisTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaNavTPAnalysisTool.h"
#include "TrigEgammaAnalysisTools/EfficiencyTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaResolutionTool.h"
#include "TrigEgammaAnalysisTools/TrigEgammaDistTool.h"

DECLARE_COMPONENT(TrigEgammaNavAnalysisTool)
DECLARE_COMPONENT(TrigEgammaNavTPAnalysisTool)
DECLARE_COMPONENT(EfficiencyTool)
DECLARE_COMPONENT(TrigEgammaResolutionTool)
DECLARE_COMPONENT(TrigEgammaDistTool)
